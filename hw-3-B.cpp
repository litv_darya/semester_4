#include <atomic>
#include <condition_variable>
#include <deque>
#include <exception>
#include <functional>
#include <future>
#include <iostream>
#include <mutex>
#include <queue>
#include <thread>
#include <utility>
#include <vector>

class MyException: public std::exception {
    virtual const char *what() const throw() {
        return "Put after shutting down...";
    }
} myException;

template <class T>
class ThreadPool;

template <class T, class Container = std::deque<T>>
class BlockingQueue {
 public:
    explicit BlockingQueue(const size_t &capacity)
            : shutted_down_(false),
              capacity_(capacity),
              put_cv_(),
              get_cv_(),
              queue_() {}

    void Put(T &&element) {
        if (!shutted_down_) {
            std::unique_lock<std::mutex> lock(own_mutex_);

            while (queue_.size() + 1 > capacity_ && !shutted_down_) {
                put_cv_.wait(lock);
            }

            queue_.push_back(std::move(element));
            get_cv_.notify_one();

        } else {
            throw myException;
        }
    }

    bool Get(T &result) {
        std::unique_lock<std::mutex> lock(own_mutex_);

        while (queue_.empty() && !shutted_down_) {
            get_cv_.wait(lock);
        }

        if (queue_.empty() && shutted_down_) {
            return false;
        }

        result = std::move(queue_.front());
        queue_.pop_front();
        put_cv_.notify_one();

        return true;
    }

    void Shutdown() {
        std::unique_lock<std::mutex>lock(own_mutex_);
        shutted_down_ = true;
        put_cv_.notify_all();
        get_cv_.notify_all();
    }

    std::atomic<bool> shutted_down_;

 private:
    friend class ThreadPool<T>;
    const size_t capacity_;
    std::condition_variable put_cv_;
    std::condition_variable get_cv_;
    std::mutex own_mutex_;
    Container queue_;
};

template <class T>
class ThreadWorker {
 public:
    explicit ThreadWorker(ThreadPool<T>& pool): pool_(pool) {}

    void operator()() {
        std::packaged_task<T()> task;
        while (pool_.queue_.Get(task)) {
            task();
        }
    }

 private:
    ThreadPool<T>& pool_;
};

template <class T>
class ThreadPool {
 public:
    ThreadPool(): queue_(num_threads_),
                  num_threads_(NumThreads()) {
        Initialize();
    }

    explicit ThreadPool(const size_t num_threads):
            queue_(num_threads),
            num_threads_(num_threads) {
        Initialize();
    }

    std::future<T> Submit(std::function<T()> task) {
        if (!queue_.shutted_down_) {
            std::packaged_task<T()> pckgd_task(task);
            std::future<T> result = pckgd_task.get_future();

            queue_.Put(std::move(pckgd_task));
            work_.notify_one();

            return result;
        } else {
            throw myException;
        }
    }

    void Shutdown() {
        queue_.Shutdown();
        work_.notify_all();
        for (size_t i = 0; i < num_threads_; ++i) {
            if (workers_[i].joinable()) {
                workers_[i].join();
            }
        }
    }

    ~ThreadPool() {
        if (!queue_.shutted_down_) {
            Shutdown();
        }
    }

 private:
    friend class ThreadWorker<T>;
    BlockingQueue<std::packaged_task<T()>> queue_;
    size_t num_threads_;
    std::vector<std::thread> workers_;
    std::condition_variable work_;
    std::mutex own_mutex_;

    void Initialize() {
        for (size_t i = 0; i < num_threads_; ++i) {
            workers_.push_back(std::thread(ThreadWorker<T>(*this)));
        }
    }

    int NumThreads() {
        int res = std::thread::hardware_concurrency();

        if (res == 0) {
            return 3;
        }

        return res;
    }
};
