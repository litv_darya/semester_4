#include <algorithm>
#include <atomic>
#include <iostream>
#include <thread>
#include <vector>

using std::vector;
using std::thread;
using std::size_t;

namespace {
    int ClosestTwoPow(size_t n_threads) {
        if (n_threads == 1)
            return 2;

        std::int64_t result_pow = 1;
        while (result_pow < n_threads) {
            result_pow = result_pow << 1;
        }

        return result_pow;
    }
}

class PetersonMutex {
 public:
    PetersonMutex(): victim_(-1) {
        wait_[0] = false;
        wait_[1] = false;
    }

    void lock(int left) {
        wait_[left] = true;
        victim_ = left;

        while (wait_[1 - left] && victim_ == left) {
            std::this_thread::yield();
        }
    }

    void unlock(int left) {
        wait_[left] = false;
    }

 private:
    std::array<std::atomic<bool>, 2> wait_;
    std::atomic<int> victim_;
};

class TreeMutex {
 public:
    explicit TreeMutex(size_t n_threads)
            :size_(ClosestTwoPow(n_threads) - 1),
             mutex_tree_(size_) {}

    void lock(size_t current_thread) {
        int index_in_tree = size_ + current_thread;
        int parent = (size_ + current_thread - 1) / 2;

        while (parent > 0) {
            mutex_tree_[parent].lock(index_in_tree % 2);
            index_in_tree = parent;
            parent = (index_in_tree - 1) / 2;
        }
        mutex_tree_[0].lock(index_in_tree % 2);
    }

    void unlock(size_t current_thread) {
        std::array<int, 2> left_right_bounds = {{(size_ - 1) / 2, size_ - 1}};

        int index_in_tree = 0;
        int current_thread_ind = (size_ + current_thread - 1) / 2;

        while (index_in_tree != current_thread_ind) {
            const auto middle = left_right_bounds[0] +
                    (left_right_bounds[1] - left_right_bounds[0]) / 2;
            const bool side = middle >= current_thread_ind;

            mutex_tree_[index_in_tree].unlock(side);
            index_in_tree = 2 * index_in_tree + 2 - side;
            left_right_bounds[side] = middle + 1 - side;
        }
        mutex_tree_[index_in_tree].unlock((current_thread + size_) % 2);
    }

 private:
    int size_;
    vector<PetersonMutex> mutex_tree_;
};

void Test(size_t num_thread, TreeMutex& mutex) {
    mutex.lock(num_thread);
    std::cout << "Hello: " << num_thread << std::endl;
    mutex.unlock(num_thread);
}

int main() {
    const size_t numThreads = 25;
    TreeMutex test_mutex(numThreads);
    std::array<thread, numThreads> test_threads;

    for (int i = 0; i < numThreads; ++i) {
        test_threads[i] = thread(Test, i, std::ref(test_mutex));
    }

    for (auto& thread : test_threads) {
        thread.join();
    }

    return 0;
}
