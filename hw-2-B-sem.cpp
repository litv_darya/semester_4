#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

class Semaphore {
 public:
    explicit Semaphore(int start_amount): current_amount_(start_amount) {
    }

    void Wait() {
        std::unique_lock<std::mutex> lock(own_mutex_);

        while (current_amount_ == 0) {
            cv_.wait(lock);
        }
        --current_amount_;
    }

    void Signal() {
        std::unique_lock<std::mutex> lock(own_mutex_);

        ++current_amount_;
        cv_.notify_one();
    }

 private:
    int current_amount_;
    std::mutex own_mutex_;
    std::condition_variable cv_;
};

class Robot {
 public:
    explicit Robot(): left_sem_(1), right_sem_(0) {}

    void StepLeft() {
        left_sem_.Wait();
        std::cout << "left" << std::endl;
        right_sem_.Signal();
    }

    void StepRight() {
        right_sem_.Wait();
        std::cout << "right" << std::endl;
        left_sem_.Signal();
    }

 private:
    Semaphore left_sem_;
    Semaphore right_sem_;
};
