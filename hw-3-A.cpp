#include <atomic>
#include <condition_variable>
#include <deque>
#include <exception>
#include <iostream>
#include <mutex>
#include <queue>


class MyException: public std::exception {
    virtual const char *what() const throw() {
        return "Put after shutting down...";
    }
} myException;

template <class T, class Container = std::deque<T>>
class BlockingQueue {
 public:
    explicit BlockingQueue(const size_t &capacity)
            : capacity_(capacity),
              put_cv_(),
              get_cv_(),
              queue_(),
              shutted_down_(false) {}

    void Put(T &&element) {
        if (!shutted_down_) {
            std::unique_lock<std::mutex> lock(own_mutex_);

            while (queue_.size() + 1 > capacity_ && !shutted_down_) {
                put_cv_.wait(lock);
            }

            queue_.push_back(std::move(element));
            get_cv_.notify_one();

        } else {
            throw myException;
        }
    }

    bool Get(T &result) {
        std::unique_lock<std::mutex> lock(own_mutex_);

        while (queue_.empty() && !shutted_down_) {
            get_cv_.wait(lock);
        }

        if (queue_.empty() && shutted_down_) {
            return false;
        }

        result = std::move(queue_.front());
        queue_.pop_front();
        put_cv_.notify_one();

        return true;
    }

    void Shutdown() {
        std::unique_lock<std::mutex>lock(own_mutex_);
        shutted_down_ = true;
        put_cv_.notify_all();
        get_cv_.notify_all();
    }

 private:
    const size_t capacity_;
    std::condition_variable put_cv_;
    std::condition_variable get_cv_;
    std::mutex own_mutex_;
    Container queue_;
    std::atomic<bool> shutted_down_;
};
