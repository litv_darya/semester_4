#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

class Robot {
 public:
    Robot(): left_(false) {}

    void StepLeft() {
        std::unique_lock<std::mutex> lock(own_mutex_);
        while (left_) {
            cv_.wait(lock);
        }
        std::cout << "left" << std::endl;
        left_ = true;
        cv_.notify_one();
    }

    void StepRight() {
        std::unique_lock<std::mutex> lock(own_mutex_);
        while (!left_) {
            cv_.wait(lock);
        }
        std::cout << "right" << std::endl;
        left_ = false;
        cv_.notify_one();
    }

 private:
    bool left_;
    std::mutex own_mutex_;
    std::condition_variable cv_;
};
