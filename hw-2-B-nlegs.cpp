#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

class Semaphore {
 public:
    Semaphore(): current_amount_(0) {}

    explicit Semaphore(int start_amount) {
        current_amount_ = start_amount;
    }

    void Wait() {
        std::unique_lock<std::mutex> lock(own_mutex_);

        while (current_amount_ == 0) {
            cv_.wait(lock);
        }
        --current_amount_;
    }

    void Signal() {
        std::unique_lock<std::mutex> lock(own_mutex_);

        ++current_amount_;
        cv_.notify_one();
    }

 private:
    int current_amount_;
    std::mutex own_mutex_;
    std::condition_variable cv_;
};

class Robot {
 public:
    explicit Robot(const std::size_t num_foots):
        num_foots_(num_foots),
        semaphores_legs_(num_foots) {
        if (num_foots_ > 0) {
            semaphores_legs_[0].Signal();
        }
    }

    void Step(const std::size_t foot) {
        semaphores_legs_[foot].Wait();
        std::cout << "foot " << foot << std::endl;
        semaphores_legs_[(foot + 1) % num_foots_].Signal();
    }

 private:
    const std::size_t num_foots_;
    std::vector<Semaphore> semaphores_legs_;
};
